# Bash MC server script

## Name
script to install Minecraft server in "Fabric" toolchain, written in bash

## Description
Sript made as an uni project, made for some personal use and to try out some automation with help of bash, to make server setup easier

## Installation
run .sh script using bash, sudo required for proper function

## License
TBD

## Project status
Currently focusing on other projects,
will have to move description from .sh file to README and translate it to english