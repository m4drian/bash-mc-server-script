#!/bin/bash

#Projekt: Skrypt BASH + sprawozdanie
#twórca: Adrian Mikołajczyk
#przedmiot: Systemy operacyjne

#WIZJA PROJEKTU:

: "
Skrypt został utworzony w celu przećwiczenia tworzenia skryptów do automatycznego tworzenia
serwerów do gier, skrypt był tworzony pod działanie z Debianem na Google Cloud Platform
 - powinien jednak działać w dowolnej dystrybucji linuxa opartej na Debianie.

Skrypt inicjalnie był oparty na przykładowym skrypcie z laboratorium.

Skrypt przygotuje i zainstaluje w kolejnych krokach lekką wersję servera Minecraft o nazwie \"fabric\",
w zakresie działania skryptu będzie: 
1 wyświetlenie informacji systemowych w celu sprawdzenia kompatybilności, oraz sprawdzenia wersji javy
2 zaktualizowanie dependencji i zainstalowanie wymaganych narzędzi do poprawnego działania serwera
3 pobranie i instalacja serwera \"fabric\", a następnie czyszczenie plików instalacyjnych
4 przygotowanie administratowa serwera
5 przygotowanie skryptu i serwera w taki sposób by można było go ponownie łatwo uruchomić za pomocą skryptu
6 usunięcie serwera
"

#PRZEBIEG DZIAŁANIA SKRYPTU:

: "
by uruchomić skrypt:
(sudo wymagane do niektórych poleceń)
sudo bash nazwa_skryptu.sh
albo
./nazwa_skryptu.sh
albo
bash nazwa_skryptu.sh

Po uruchomieniu skryptu użytkownikowi wyświeli się menu z funkcjalmi skryptu oraz krótkim opisem jak powinno się
poprawnie używać skryptu

Następnie jak pisze w menu skryptu należy uruchomić kroki skryptu po kolei:

pierwszy krok odpala funkcję f_system_info_and_testing sprawdza informacje o procesorze, dyskach komputera oraz 
nazwę systemu operacyjnego, w funkcji są również pozostałości po testach pętli w bash

kolejny krok uruchamia f_update_dependencies które aktualizuje dependencje co będzie potrzebne na nowo
utworzonej maszynie by później zainstalować potrzebne narzędzia

kolejny krok uruchamia f_install_tools instaluje to narzędzia:
java, curl, wget, adduser, screen

kolejny krok uruchamia f_install_server który sprawdza czy serwer już istnieje, a następnie pobiera plik .jar
instalatora serwera i uruchamia instalator, następnie usuwane są niepotrzebne pliki po instalacji
java curl i wget były wymagane by ten krok działał poprawnie


kolejny krok używa f_rename w celu przeniesieniu plików po instalatorze do folderu gdzie ma być serwer
a następnie zmienia nazwy pobranych plików na łatwiejsze do rozpoznania nazwy,
w ustawieniach serwera zostaje napisana linijka informująca o lokacji niezmodyfikowanej wersji serwera.

kolejny krok używa f_start_server co uruchamia pobrany serwer, przy pierwszym uruchomieniu 
serwer przygotuje potrzebne pliki, a następnie od razu przerwie działanie, będzie wymaganie podpisanie euli

następnie podpisywana jest eula za pomocą f_sign_eula

ostatni istotny krok tworzy dedykowane konto admina używając polecenia adduser (ładniejszy interfejs od useradd)
przy pomocy f_create_server_admin, w tym kroku przeniesione są również pliki serwera do folderu nowego użytkownika,
od tej pory praca z serwerem nie będzie wymagać uprawnień sudo


reszta kroków jest opcjonalna wedle użytkownika:

	f_start_server - pozwoli ponownie uruchomić serwer w aktualnym terminalu
			
	f_run_server_with_screen - odpala serwer w sesji narzędzia \"screen\", więcej we wnioskach
			
	f_delete_server - czyści wszystkie pliki serwera (zostawia konto admina)


"

#NAPOTKANE PROBLEMY ORAZ WNIOSKI:

: "
problemy i wnioski z problemów:
	1 root oraz server_admin
	na początku skrypt był pisany z założeniem uruchamiania wszystkiego jako root,
	przez takie działanie po stosowaniu wszędzie aliasu $HOME w celu instalacji serwera
	wszystkie pliki zostały zainstalowane w folderze /root co uniemożliwiało innym userom
	kożystanie z servera oraz było oczywistą luką bezpieczeństwa.
	W celu rozwiązania tego błędu musiałem stworzyć dedykowanego uzytkownika server_admin do obsługi serwera w celu
	przeniesienia plików serwera do folderu home nowego użytkownika
	
	2 screen 
	kolejnym problemem był fakt że uruchomienie serwera z poziomu skryptu zajmuje całe okno konsoli
	uniemożliwaiając uzywanie konsoli gdy serwer chodzi, jest to problem na GCP jako że niektóre kontenery
	na tej platformie nie pozwalają na przełączanie między konsolami, dodatkowo wyłączenie maszyny 
	powodowało wyłączenie serwera z utratą wszystkich informacji o aktualnej sesji serwera, powodowało również
	korupcję zapisów świata gry.
	Wszystkie te problemy rozwiązało dodanie narzędzia screen które pozwala oddzielić sesję serwera od aktualnie używanej
	konsoli, dodatkowo dana sesja może być zatrzymana i wznowiona po ponownym uruchomieniu systemu
	
	3 przenoszenie plików po instalatorze
	instalator \"fabric\" niestety nie daje opcji wyboru miejsca instalacji plików serwera, 
	instaluje wszystko w miejscu gdzie uruchamiany jest skrypt, jako że skrypt jest stworzony 
	z zamysłem uruchomienia go w dowolnym miejscu w systemie musiałem ręcznie poprzenosić utworzone przez
	instalator pliki do wyznaczonych folderów

ogólne wnioski:
	Zadanie pozwoliło mi dobrze przećwiczyć używanie najistotniejszych funkcji pisania skryptów w BASH,
	napotkane problemy skłoniły mnie do poszukania narzędzi pozwalających je rozwiązać.
	Wszystkie założenia projektu zostały spełnione, skrypt był tworzony z zamysłem użytku osobistego 
	i świetnie się do tego nada.
	
"


#KOD PROJEKTU:

#[variable declaration]

option="1"

delay_time=1

jdk_version="openjdk-17-jre-headless"

fabric_version="https://maven.fabricmc.net/net/fabricmc/fabric-installer/0.11.1/fabric-installer-0.11.1.jar"

server_location="/home/server_admin"

testA=2

testB=3

testSUM=0

#[function declaration]

function f_system_info_and_testing()
{
	#system name
	uname -a
	
	echo -e "\n"
	
	#list CPU information
	lscpu
	
	echo -e "\n"
	
	#testing for while arytmetic operations
	until [ $testB -le 0 ]
	do
		testSUM=$(( testSUM + (testA ^ testB) ))
		testB=$(( testB - 1 ))
	done
	echo $testSUM
	
	for n in {2..5};
	do
		echo -e "Welcome for the $n time!"
	done
	
	#lists information about all available or the specified block devices
	lsblk
}

function f_update_dependencies()
{
	sudo apt-get update
}

function f_install_tools()
{
	
	#dependencies required to install and run fabric server
	sudo apt install "$jdk_version" curl wget adduser
	
	#divides a physical terminal into multiple virtual sessions, stops and summarizes the activity in them.
	#with this user will be able to save servers state, and easily resume it even when server's machine was shut down
	sudo apt install screen
}

function f_install_server()
{
	server_location="$HOME/fabric"

	if [[ ! -d "$HOME/fabric" ]]; then
		mkdir "$HOME/fabric"
	fi
	
	curl -o "$HOME/fabric/installer.jar" "$fabric_version"
	
	java -jar "$HOME/fabric/installer.jar" server -mcversion 1.18.2 -downloadMinecraft
	
	echo "^Ignore previous message, follow to step 6!!!"
	
	rm "$HOME/fabric/installer.jar"
}

function f_rename()
{
	server_location="$HOME/fabric"

	if [[ ! -f "$HOME/fabric/vanilla.jar" ]];then #check if step was already done
		if [[ -f "$HOME/fabric/server.jar" ]]; then
			mv -f "$HOME/fabric/server.jar" "$HOME/fabric/vanilla.jar"
		fi
		if [[ -f "./server.jar" ]]; then
			mv -f "./server.jar" "$HOME/fabric/vanilla.jar"
		fi
	fi
	
	if [[ -f "$HOME/fabric/fabric-server-launch.jar" ]]; then
		mv -f "$HOME/fabric/fabric-server-launch.jar" "$HOME/fabric/server.jar"
	fi
	
	if [[ -f "./fabric-server-launch.jar" ]]; then
		mv -f "./fabric-server-launch.jar" "$HOME/fabric/server.jar"
	fi
	
	if [[ -d "./libraries" ]]; then
		mv "./libraries" "$HOME/fabric/"
	fi
	
	echo "serverJar=vanilla.jar" > "$HOME/fabric/fabric-server-launcher.properties"
}

function f_start_server()
{
	cd "$server_location/fabric" || 
		{ echo "Couldn't change to HOME directory, rerun scipt!"; exit "$ERRCODE"; }
		
	java -Xms500m -Xmx2G -jar "$server_location/fabric/server.jar" nogui
}


function f_sign_eula()
{
	sed -i "s/eula=false/eula=true/" "$server_location/fabric/eula.txt"
}

#make user called Fabric
#change files and folders owner to Fabric, give him execute rights
#move server files to 'server_admin' home folder
function f_create_server_admin
{
	#check if user exists, if not create him
	id -u server_admin &>/dev/null || sudo adduser server_admin
	
	server_location="/home/server_admin"
	
	#move server's files
	if [[ -d "/root/fabric" ]]; then
		mv "/root/fabric" "$server_location"
	fi
	
	#change server ownership to new user
	chown -R server_admin:server_admin "$server_location/fabric"
	
	#full rights for user, read write rights for group, no rights for others
	# run on entire fabric folder
	sudo chmod -R 760 "$server_location/fabric"
}

#install screen
#run server using screen utility
#screen -d -m -S fabric bash -c 'cd $HOME/fabric && java -Xms500m -Xmx2G -jar server.jar'
function f_run_server_with_screen
{
	echo -e "check for running screen sessions with 'screen -ls' change session with 'screen -r fabric'\n 'Ctrl+a+d' - to detach screen again"
	
	#cleanup after previous sessions
	screen -wipe
	screen -X -S fabric quit
	
	screen -d -m -S fabric bash -c 'cd /home/server_admin/fabric; java -Xms500m -Xmx2G -jar server.jar; exec sh'
}

function f_delete_server()
{
	rm -rf "$server_location/fabric"
}
	
#[main script loop]

function main() {

	echo "Welcome!"

	until [ "$option" -eq "0" ]; do
	echo "
	***********************************************
	* Script to create a server in Minecraft
	* using Fabric toolchain
	*
	*	!!THIS SCRIPT MUST BE RUN AS SUDO FOR STEPS 2-5 and 8!!
	*	!!'screen' (s) should be run as server's admin!!
	*
	*	!!PLEASE RUN IN ORDER 1-8!!
	*
	* 1 - Gather system information, test bash arytm operations
	* 2 - Update dependencies [sudo only]
	* 3 - Download java ($jdk_version) and other required utilities [sudo only]
	* 4 - Install Fabric server [sudo only]
	* 5 - Rename server to 'server.jar' [sudo only]
	* 6 - Start server (if installed previously, just run this step)
	* 7 - Agree to eula (after server installation)
	* 8 - Create server admin
	* s - Start server using 'screen' utility [server_admin recommended]
	* d - WARNING: Delete server
	* ANY - Leave script
	***********************************************
	"
	echo "Choose option: (any button outside of range will end the script) > "
	read -r option
		case "$option" in
			"1")	
				f_system_info_and_testing 
				
				echo -e "\n"
				
				java --version
				
				echo -e "\n"
				
				echo -e "If you already have java over version $jdk_version,\n skip java installation (skip step 3),\n if this command printed an error - DONT skip step 3"
				
				read -n 1 -s -r -p "Step 1 done, Press any key to continue..." ;;
			"2")  
				f_update_dependencies 
				
				read -n 1 -s -r -p "Step 2 done, Press any key to continue..." ;;
			"3")  
				f_install_tools
				
				read -n 1 -s -r -p "Step 3 done, Press any key to continue..." ;;
			"4")  
				f_install_server
				
				read -n 1 -s -r -p "Step 4 done, Press any key to continue..." ;;
			"5")  
				f_rename
				
				echo "old server is now called 'vanilla.jar' and Fabric one is called 'server.jar'"
				
				read -n 1 -s -r -p "Step 5 done, Press any key to continue..." ;;
			"6")  
				f_start_server 
				
				read -n 1 -s -r -p "Step 6 done, Press any key to continue..." ;;
			"7")  
				f_sign_eula
				
				read -n 1 -s -r -p "Step 7 done, Press any key to continue..." ;;
			"8")  
				f_create_server_admin
				
				echo -e "Step 8 done, User 'server_admin' prepared, it is recommended to rerun this script as 'server_admin' user"
				read -n 1 -s -r -p "Press any key to continue..." ;;
			"s")
				f_run_server_with_screen
				
				read -n 1 -s -r -p "Server is running in screen's 'fabric' session, Press any key to continue..." ;;
			"d")  
				f_delete_server 
				
				read -n 1 -s -r -p "DELETION FINISHED, Press any key to continue..." ;;
			*) 
				echo "Exiting the script"; exit ;;
		esac
	sleep "$delay_time"
	done
}

main

